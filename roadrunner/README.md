# Roadrunner

An experimental [roadrunner](https://github.com/spiral/roadrunner) image built on top of `1maa/php:8.0` with XDebug enabled.

## Usage

Overwrite `/.rr.yaml` with your own configuration.
